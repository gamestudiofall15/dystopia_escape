﻿#pragma strict
import UnityEngine.UI; // Needed for ui text to work

static var pickedUp = 0;

var needed = 4; 
var scoreText : Text;


function Update () {
		
	Debug.Log("TOTAL PICKED UP = " + pickedUp);
		
	scoreText.text = pickedUp.ToString() + " / " + needed.ToString();
		
	// Check if player collect the needed amount
	if(pickedUp == needed){
	
		this.gameObject.GetComponent.<Renderer>().enabled = false; // turn off graphics
		this.gameObject.GetComponent.<Collider>().enabled = false; // turn off collider
	
	}
}