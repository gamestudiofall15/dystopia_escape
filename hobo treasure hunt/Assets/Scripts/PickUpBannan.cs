﻿using UnityEngine;
using System.Collections;

public class PickUpBannan : MonoBehaviour {

	void OnTriggerEnter ( Collider other) {
		
		// Tell Hud class you picked up item1	
		HudItems.item1 =true;

		// Remove this gameobject attached to this script
		Destroy(this.gameObject);
	}
}
